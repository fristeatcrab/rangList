package main

import (
	"fmt"
	"strings"
)

/*
# Task: Implement a class named 'RangeList'
 # A pair of integers define a range, for example: [1, 5).
 This range includes integers: 1, 2, 3, and 4.
  # A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
   # NOTE: Feel free to add any extra member variables/functions you like.
    class RangeList def add(range)
	# TODO: implement add end def remove(range)
	 # TODO: implement remove end def print
	  # TODO: implement
	   print end
	    end
	   rl = RangeList.new rl.add([1, 5])
	  rl.print
	  // Should display: [1, 5)
	  rl.add([10, 20])
	  rl.print
*/

type RangeList struct {
	list [][2]int
}

func (rangeList *RangeList) Add(increased [2]int) [][2]int {
	if len(rangeList.list) == 0 {
		rangeList.list = append(rangeList.list, increased)
	} else {
		for i := 0; i < len(rangeList.list); i++ {
			if (rangeList.list[i][1] >= increased[0] && rangeList.list[i][0] <= increased[0]) || (rangeList.list[i][1] >= increased[1] && rangeList.list[i][0] <= increased[1]) {
				re := merge(rangeList.list[i], increased)
				rangeList.list[i] = re
				return rangeList.list
			}
		}
		rangeList.list = append(rangeList.list, increased)
		var data []int
		for _, arry := range rangeList.list {
			for _, number := range arry {
				data = append(data, number)
			}
		}
		var result [][2]int
		resultArry := quickSort(data, 0, len(data))
		fmt.Println(resultArry)
		group := len(resultArry) / 2
		for i := 1; i <= group; i++ {
			var tmp [2]int
			tmp[0] = resultArry[2*i-2]
			tmp[1] = resultArry[i*2-1]
			result = append(result, tmp)
		}
		rangeList.list = result
		return rangeList.list
	}

	return rangeList.list
}

func merge(old, new [2]int) [2]int {
	var data []int
	for i := 0; i < len(old); i++ {
		data = append(data, old[i])
	}
	for i := 0; i < len(new); i++ {
		data = append(data, new[i])
	}
	resultArry := quickSort(data, 0, len(data))
	var tmp [2]int
	tmp[0] = resultArry[0]
	tmp[1] = resultArry[len(resultArry)-1]
	return tmp
}

func remove(a []int) []int {
	tmp := make(map[int]bool, 0)
	tmp1 := make(map[int]int, 0)
	var times []int
	for _, n := range a {
		if !tmp[n] {
			tmp[n] = true
			tmp1[n] += 1
		} else {
			tmp1[n] += 1
		}
	}
	for m := range tmp1 {
		if tmp1[m] <= 1 {
			times = append(times, m)
		}
		if tmp1[m] >= 3 {
			times = append(times, m)
		}
	}
	return times

}

func (rangeList *RangeList) Print() {
	stringList := make([]string, len(rangeList.list))
	for i, v := range rangeList.list {
		stringList[i] = fmt.Sprintf("[%d, %d)", v[0], v[1])
	}
	fmt.Println(strings.Join(stringList, " "))
	return
}

func (rangeList *RangeList) Remove(reduce [2]int) (result [][2]int) {
	for i := 0; i < len(rangeList.list); i++ {
		if (rangeList.list[i][0] == reduce[0]) || (rangeList.list[i][1] == reduce[1]) {
			rangeList.list = append(rangeList.list, reduce)
			var data []int
			for _, arry := range rangeList.list {
				for _, number := range arry {
					data = append(data, number)
				}
			}

			resultArry := quickSort(data, 0, len(data))
			resultArry = remove(resultArry)
			resultArry = quickSort(resultArry, 0, len(resultArry))
			fmt.Println(resultArry)
			group := len(resultArry) / 2
			for i := 1; i <= group; i++ {
				var tmp [2]int
				tmp[0] = resultArry[2*i-2]
				tmp[1] = resultArry[i*2-1]
				result = append(result, tmp)
			}
			rangeList.list = result
			return rangeList.list
		}
		if (rangeList.list[i][1] > reduce[0] && rangeList.list[i][0] < reduce[0]) && (rangeList.list[i][1] > reduce[1] && rangeList.list[i][0] < reduce[1]) {
			rangeList.list = append(rangeList.list, reduce)
			var data []int
			for _, arry := range rangeList.list {
				for _, number := range arry {
					data = append(data, number)
				}
			}
			resultArry := quickSort(data, 0, len(data))

			group := len(resultArry) / 2
			for i := 1; i <= group; i++ {
				var tmp [2]int
				tmp[0] = resultArry[2*i-2]
				tmp[1] = resultArry[i*2-1]
				result = append(result, tmp)
			}
			rangeList.list = result
			return rangeList.list
		}
	}
	rangeList.list = append(rangeList.list, reduce)
	var data []int
	for _, arry := range rangeList.list {
		for _, number := range arry {
			data = append(data, number)
		}
	}
	resultArry := quickSort(data, 0, len(data))
	group := len(resultArry) / 2
	for i := 1; i <= group; i++ {
		var tmp [2]int
		tmp[0] = resultArry[2*i-2]
		tmp[1] = resultArry[i*2-1]
		if tmp[1] <= reduce[0] {
			result = append(result, tmp)
		}
		if tmp[0] >= reduce[1] {
			result = append(result, tmp)
		}

	}
	rangeList.list = result
	return rangeList.list

}

func swap(a int, b int) (int, int) {
	return b, a
}

//
func partition(aris []int, begin int, end int) int {
	pvalue := aris[begin]
	i := begin
	j := begin + 1
	for j < end {
		if aris[j] < pvalue {
			i++
			aris[i], aris[j] = swap(aris[i], aris[j])
		}
		j++
	}
	aris[i], aris[begin] = swap(aris[i], aris[begin])
	return i
}

// 	快排
func quickSort(arry []int, begin int, end int) []int {
	if begin+1 < end {
		mid := partition(arry, begin, end)
		quickSort(arry, begin, mid)
		quickSort(arry, mid+1, end)
	}
	return arry
}

func main() {
	//p:=[]int{1 ,5 ,10 ,20,20, 20,20,21}
	//reduceNumber(p)
	rl := RangeList{}
	rl.Add([2]int{1, 5})
	rl.Print() // Should display: [1, 5)
	rl.Add([2]int{10, 20})
	rl.Print() // Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 20})
	rl.Print() // Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 21})
	rl.Print() // Should display: [1, 5) [10, 21)
	rl.Add([2]int{2, 4})
	rl.Print() // Should display: [1, 5) [10, 21)
	rl.Add([2]int{3, 8})
	rl.Print() // Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 10})
	rl.Print() // Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 11})
	rl.Print() // Should display: [1, 8) [11, 21)
	rl.Remove([2]int{15, 17})
	rl.Print() // Should display: [1, 8) [11, 15) [17, 21)
	rl.Remove([2]int{3, 19})
	rl.Print() // Should display: [1, 3) [19, 21)

}
